TDD OOP Exercise
================

Use the provided scripts to compile the code and run the tests. Use the errors
reported to guide you towards the required solution. You can find a video
implementing the solution [here](https://youtu.be/svkyGv7dJ8E). The complete
solutions can also be found on the branch `completed_solutions` in the repo.
