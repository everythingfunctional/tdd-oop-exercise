#!/bin/bash

set -e

rm -f first_circle_interface.o first_circle_interface.mod first_circle_interface.smod first_circle_implementation.o first_circle_interface@first_circle_implementation.smod first_test.o first_test
gfortran -c first_circle_interface.f90
gfortran -c first_circle_implementation.f90
gfortran -c first_test.f90
gfortran first_circle_interface.o first_circle_implementation.o first_test.o -o first_test
./first_test
