#!/bin/bash

set -e

rm -f second_circle_interface.o second_circle_interface.mod second_circle_interface.smod second_circle_implementation.o second_circle_interface@second_circle_implementation.smod second_test.o second_test
gfortran -c second_circle_interface.f90
gfortran -c second_circle_implementation.f90
gfortran -c second_test.f90
gfortran second_circle_interface.o second_circle_implementation.o second_test.o -o second_test
./second_test
