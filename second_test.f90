program main
  use second_circle_interface, only : circle_t
  implicit none

  real, parameter :: pi=3.141592654, dia=2.

  type(circle_t) circle

  call circle%set_diameter( diameter=dia )
  call print_area(circle)

  call assert(circle%area() == pi*(dia**2)/4.)

  print *,"Test passed."
contains

  subroutine assert(assertion)
    logical, intent(in) :: assertion
    if (.not. assertion) error stop "Test failed."
  end subroutine

  subroutine print_area(some_circle)
    type(circle_t), intent(in) :: some_circle
    call assert(some_circle%user_defined())
    print *,some_circle%area()
  end subroutine

end program
