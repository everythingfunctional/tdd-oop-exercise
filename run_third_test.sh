#!/bin/bash

set -e

rm -f object_interface.o object_interface.mod object_interface.smod object_implementation.o object_interface@object_implementation.smod third_circle_interface.o third_circle_interface.mod third_circle_interface.smod third_circle_implementation.o third_circle_interface@third_circle_implementation.smod third_test.o third_test
gfortran -c object_interface.f90
gfortran -c object_implementation.f90
gfortran -c third_circle_interface.f90
gfortran -c third_circle_implementation.f90
gfortran -c third_test.f90
gfortran object_interface.o object_implementation.o third_circle_interface.o third_circle_implementation.o third_test.o -o third_test
./third_test
